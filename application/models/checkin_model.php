<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkin_model extends CI_Model
{
    /**
     * Holds an array of tables used
     *
     * @var array
     **/

    public $tables = array();

    public $amount;
    public $invoice_url;


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->config('ion_auth', TRUE);
        $this->load->helper('cookie');
        $this->load->helper('date');

        // initialize db tables data
        $this->tables = $this->config->item('tables', 'checkin');
    }

    function insert($item)
    {
        return $this->db->insert('checkin', $item);
    }
    
    function getList($userid){
        $this->db->from('checkin');
        $this->db->where('user_id', $userid );
        $query = $this->db->get();
        return $query->result_array();
    }

    function getLatest($userid){
        $this->db->from('checkin');
        $this->db->where('user_id', $userid );
        $this->db->order_by("id", "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result_array();
    }
}