<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verify_model extends CI_Model
{
    /**
     * Holds an array of tables used
     *
     * @var array
     **/

    public $tables = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->helper('date');
        $this->load->library('session');
    }

    function getList($id){
        $this->db->from('user_verify');
        $this->db->where('user_id', $id );
        $data = $this->db->get()->result_array();
        return $data;
    }

    function insert($data){
        return $this->db->insert('user_verify', $data);
    }
    function update($data, $id){
        return $this->db->update('user_verify', $data, array('id' => $id ));
    }
}
