<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message_model extends CI_Model
{
    /**
     * Holds an array of tables used
     *
     * @var array
     **/

    public $tables = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
//        $this->load->config('messages', TRUE);
        $this->load->helper('cookie');
        $this->load->helper('date');

        // initialize db tables data
//        $this->tables = $this->config->item('tables', 'messages');
    }

    function getList($userid){
        $this->db->from('messages');
        $this->db->where('user_id', $userid );
        $query = $this->db->get();
        return $query->result_array();
    }
}