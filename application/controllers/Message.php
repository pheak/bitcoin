<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('form', 'url'));
        $this->load->library( 'session');
        $this->load->database();
        $this->load->model('ion_auth_model');
        $this->load->model('Message_model');
        $this->load->model('News_model');
    }


    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } else {
            $id = $this->session->userdata('user_id');
            $this->db->from('users');
            $this->db->where('id', $id );
            $user = $this->db->get()->result()[0];
            $this->data['data'] = array(
                'img_url'       => $user->img_url,
                'verify'       => $user->verify
            );

            // Get Check In photos
            $this->data['messages'] = $this->Message_model->getList($id);
            $this->data['msg_count'] = count($this->Message_model->getList($id));
            $this->data['title'] = "Message";
            $this->data['username'] = $this->session->userdata('identity');
            $this->data['email'] = $this->session->userdata('email');
            $this->load->view('message/mymessage', $this->data);
        }
    }

    public function read($new_id){
        $user_id = $this->session->userdata('user_id');
        $data = array(
            'user_id' => $user_id,
            'news_id' => $new_id,
            'read_date' => date("Y-m-d H:i:s")
        );
        $this->News_model->readMessage($data);
    }
}
