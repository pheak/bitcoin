<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->library('session');
        $this->load->model('Message_model');
        $this->load->model('News_model');
        $this->load->model('Checkin_model');
        $this->load->model('Feedback_model');
    }


    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }  else {
            $id = $this->session->userdata('user_id');
            $this->db->from('users');
            $this->db->where('id', $id );
            $user = $this->db->get()->result()[0];
            $this->data['data'] = array(
                'username'      => $user->username,
                'email'         => $user->email,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'company'       => $user->company,
                'phone'         => $user->phone,
                'sex'           => $user->sex,
                'age'           => $user->age,
                'homeno'        => $user->homeno,
                'street'        => $user->street,
                'phum'          => $user->phum,
                'sangkat'       => $user->sankat,
                'khan'          => $user->khan,
                'city'          => $user->city,
                'img_url'       => $user->img_url,
                'verify'        => $user->verify
            );
            $count_day = 30;
            $invoice = $this->Checkin_model->getLatest($id);
            if(count($invoice)>0){
                $datetime1 = new DateTime($invoice[0]["date"]);
                $datetime2 = new DateTime();
                $interval = $datetime1->diff($datetime2);
                $count_day = ($count_day - $interval->format('%a'))-1;
            }

            $this->data['count_booking'] = $count_day;
            $this->data['news'] = $this->News_model->getList($id);
            $this->data['title'] = "Dashboard";
            $this->data['msg_count'] = count($this->Message_model->getList($id));
            $this->data['username'] = $this->session->userdata('identity');
            $this->data['email'] = $this->session->userdata('email');
            $this->data['feedback_mgs'] = $this->session->flashdata('feedback_mgs');
            $this->load->view('template/header', $this->data);
            $this->load->view('dashboard/dashboard');
            $this->load->view('template/footer', $this->data);
        }
    }

    public function feedback()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }  else {
            $id = $this->session->userdata('user_id');
            $img_url = "";

            $data = array(
                'user_id'   => $id,
                'message'   => $this->input->post('message'),
                'date'      => date("Y-m-d H:i:s")
            );

            if($this->Feedback_model->insert($data))
            {
                $this->session->set_flashdata('feedback_mgs', 'Your message sent successfull!');
                redirect('/');
            } else {
                redirect('/');
            }
        }
    }
}
