<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('Message_model');
        $this->load->model('News_model');
        $this->load->model('Checkin_model');
        $this->load->model('Verify_model');
    }


    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } else {
            $id = $this->session->userdata('user_id');
            $this->db->from('users');
            $this->db->where('id', $id );
            $user = $this->db->get()->result()[0];
            $this->data['data'] = array(
                'img_url'       => $user->img_url,
                'verify'        => $user->verify
            );

            $verify = $this->Verify_model->getList($id);
            $this->data['verify'] = $verify;
            $this->data['news'] = $this->News_model->getList($id);
            $this->data['title'] = "User Account Verify";
            $this->data['msg_count'] = count($this->Message_model->getList($id));
            $this->data['username'] = $this->session->userdata('identity');
            $this->data['email'] = $this->session->userdata('email');
            $this->load->view('verify/userverify', $this->data);
        }
    }

    public function update(){
        $id = $this->session->userdata('user_id');
        $verify = $this->Verify_model->getList($id);
        $img_url1 = "";
        $img_url2 = "";
        $img_url3 = "";
        $img_url4 = "";
        for($i=1; $i<=4; $i++){
            if(!empty($_FILES['image'.$i]['name'])) {
                $img_url = $this->upload('image'.$i);
                $data = array(
                    'user_id'   => $id,
                    'img_url'   => $img_url,
                    'date'      => date("Y-m-d H:i:s"),
                    'verify'    => 0
                );
                if(!empty($verify[$i-1]))
                    $this->Verify_model->update($data, $verify[$i-1]['id']);
                else
                    $this->Verify_model->insert($data);
            }
        }
        $this->session->set_flashdata('verify', 'Verify updated!');
        redirect('verify', 'refresh');

    }

    function upload($image){
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $timestamp = time() - date('Z');
        $config['file_name'] =  $timestamp.'-'.$_FILES[$image]['name'];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload($image))
        {
            $error = array('error' => $this->upload->display_errors());
            return "";
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            return $data['upload_data']['file_name'];
        }
    }
}
