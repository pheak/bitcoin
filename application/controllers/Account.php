<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('form', 'url'));
        $this->load->library( 'session');
        $this->load->database();
        $this->load->model('ion_auth_model');
        $this->load->model('Message_model');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } else {
            $id = $this->session->userdata('user_id');
            $this->db->from('users');
            $this->db->where('id', $id );
            $user = $this->db->get()->result()[0];
            $this->data['data'] = array(
                'username'      => $user->username,
                'email'         => $user->email,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'company'       => $user->company,
                'phone'         => $user->phone,
                'sex'           => $user->sex,
                'age'           => $user->age,
                'homeno'        => $user->homeno,
                'street'        => $user->street,
                'phum'          => $user->phum,
                'sankat'        => $user->sankat,
                'khan'          => $user->khan,
                'city'          => $user->city,
                'img_url'       => $user->img_url,
                'verify'        => $user->verify,
                'sponser'       => $user->sponser,
                'country'       => $user->country
            );
            $this->data['title'] = "My Account";
            $this->data['msg_count'] = count($this->Message_model->getList($id));
            $this->data['username'] = $this->session->userdata('identity');
            $this->data['email'] = $this->session->userdata('email');
            $this->data['account_mgs'] = $this->session->flashdata('account_mgs');
            $this->load->view('account/myaccount', $this->data);
        }
    }

    function update(){
        $id = $this->session->userdata('user_id');
        $img_url = "";
        if($_FILES['image']['name']) {
            $img_url = $this->upload();
        }
        $data = array(
            'email'      => $this->input->post('email'),
            'company'    => $this->input->post('company'),
            'phone'      => $this->input->post('phone'),
            'sex'        => $this->input->post('sex'),
            'age'        => $this->input->post('age'),
            'homeno'     => $this->input->post('homeno'),
            'street'     => $this->input->post('street'),
            'phum'       => $this->input->post('phum'),
            'sankat'     => $this->input->post('sankat'),
            'khan'       => $this->input->post('khan'),
            'city'       => $this->input->post('city'),
            'sponser'    => $this->input->post('sponser'),
            'country'    => $this->input->post('country'),

        );
        if($_FILES['image']['name']) {
            $img_url = $this->upload();
            $data["img_url"] = $img_url;
        }
        // check to see if we are updating the user
        if($this->ion_auth->update($id, $data))
        {
            $this->session->set_flashdata('account_mgs', 'Your account updated successfull!');
            redirect('account', 'refresh');
        } else {
            redirect('account', 'refresh');
        }
    }

    function upload(){
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $timestamp = time() - date('Z');
        $config['file_name'] =  $timestamp.'-'.$_FILES['image']['name'];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('image'))
        {
            $error = array('error' => $this->upload->display_errors());
            return "";
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            return $data['upload_data']['file_name'];
        }
    }
}
