<!-- // Wrapper END -->

<!-- Modernizr -->
<script src="<?php echo base_url() ?>assets/common/theme/scripts/plugins/system/modernizr.js"></script>

<!-- Bootstrap -->
<script src="<?php echo base_url() ?>assets/common/bootstrap/js/bootstrap.min.js"></script>

<!-- Uniform Forms Plugin -->
<script
    src="<?php echo base_url() ?>assets/common/theme/scripts/plugins/forms/pixelmatrix-uniform/jquery.uniform.min.js"></script>


<!-- Common Demo Script -->
<script src="<?php echo base_url() ?>assets/common/theme/scripts/demo/common.js?1374506526"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
<script type="text/javascript">
    $(document).ready(function () {
        $("a#news").bind('click', function () {
            var id = $(this).attr('data-id');
            $(this).addClass("message-read");
            $.ajax({
                url: "<?php echo base_url() ?>message/read/"+id,
                success: function(result){
                }
            });
        });
    });
</script>
</body>
</html>