<!-- Main Container Fluid -->
<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">

    <!-- Sidebar menu & content wrapper -->
    <div id="wrapper">

        <!-- Sidebar Menu -->
        <div id="menu" class="hidden-print">

            <!-- Brand -->
            <a href="#" class="appbrand"><span class="text-primary">SB</span> <span>Team</span></a>

            <!-- Scrollable menu wrapper with Maximum height -->
            <div class="slim-scroll" data-scroll-height="800px">

                <!-- Menu Toggle Button -->
                <button type="button" class="btn btn-navbar">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <!-- // Menu Toggle Button END -->

                <!-- Sidebar Profile -->
				<span class="profile center">
					<a href="<?php echo base_url("account"); ?>">
                        <?php if($data['img_url']){ ?>
                            <img src="<?php echo base_url() ?>assets/uploads/<?php echo $data['img_url'];?>" alt="Avatar"/>
                        <?php } else { ?>
                            <img src="http://aminoapps.com/static/img/user-icon-placeholder.png" alt="Avatar"/>
                        <?php } ?>
                    </a>

				</span>
                <!-- // Sidebar Profile END -->

                <!-- Menu -->
                <ul>

                    <li class="<?php echo ($title==="Dashboard")?'active':''; ?>"><a href="<?php echo base_url("/"); ?>" class="glyphicons dashboard"><i></i><span>Dashboard</span></a></li>
                    <li class="<?php echo ($title==="My Account")?'active':''; ?>"><a href="<?php echo base_url("account"); ?>" class="glyphicons user"><i></i><span>My Account</span></a></li>
                    <?php if($data['verify']==0) {?>
                        <li class="<?php echo ($title==="User Account Verify")?'active':''; ?>"><a href="<?php echo base_url("verify"); ?>" class="glyphicons check"><i></i><span>Verify</span></a></li>
                    <?php } ?>
                    <?php if($data['verify']==1) {?>
                        <li class="<?php echo ($title==="Check-In")?'active':''; ?>"><a href="<?php echo base_url("checkin"); ?>" class="glyphicons usd"><i></i><span>Check In</span></a></li>
                    <?php } ?>
                    <li class="<?php echo ($title==="My Photos")?'active':''; ?>"><a href="<?php echo base_url("photos"); ?>" class="glyphicons camera"><i></i><span>My Photos</span></a></li>

                </ul>
                <div class="clearfix"></div>
                <!-- // Menu END -->
                <div class="menu-hidden-element alert alert-primary">
                    <a class="close" data-dismiss="alert">&times;</a>
                    <p>Integer quis tempor mi. Donec venenatis dui in neque fringilla at iaculis libero ullamcorper. In
                        velit sem, sodales id hendrerit ac, fringilla et est.</p>
                </div>
            </div>
            <!-- // Scrollable Menu wrapper with Maximum Height END -->
        </div>
        <!-- // Sidebar Menu END -->
        <!-- Content -->
        <div id="content">
            <!-- Top navbar -->
            <div class="navbar main">
                <!-- Menu Toggle Button -->
                <button type="button" class="btn btn-navbar pull-left visible-phone">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <!-- // Menu Toggle Button END -->
                <!-- Full Top Style -->
                <ul class="topnav pull-left">
                    <li class="active"><a href="#" class="glyphicons"><i></i> <?php echo $title;?></a></li>
                </ul>
                <!-- // Full Top Style END -->
                <!-- Top Menu Right -->
                <ul class="topnav pull-right hidden-phone">
                    <!-- Profile / Logout menu -->
                    <li class="account dropdown dd-1">
                        <a data-toggle="dropdown" href="#" class="glyphicons logout lock"><span
                                class="hidden-tablet hidden-phone hidden-desktop-1"><?php echo $username; ?></span><i></i></a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?php echo base_url("account"); ?>" class="glyphicons cogwheel">Settings<i></i></a></li>
                            <li><a href="<?php echo base_url("photos"); ?>" class="glyphicons camera">My Photos<i></i></a></li>
                            <li class="profile">
							<span>
								<span class="heading">Profile <a href="<?php echo base_url("account"); ?>" class="pull-right text-primary text-weight-regular">edit</a></span>
								<a href="<?php echo base_url("account"); ?>" class="img thumb">
                                    <?php if($data['img_url']){ ?>
                                    <img src="<?php echo base_url() ?>assets/uploads/<?php echo $data['img_url'];?>" width="50px" alt="Avatar"/>
                                    <?php } else { ?>
                                    <img src="http://aminoapps.com/static/img/user-icon-placeholder.png" width="50px" alt="Avatar"/>
                                    <?php } ?>

                                </a>
								<span class="details">
									<a href="<?php echo base_url("account"); ?>" class="strong text-regular"><?php echo $username; ?></a>
                                    <?php echo $email; ?>
								</span>
								<span class="clearfix"></span>
							</span>
                            </li>
                            <li>
							<span>
								<a class="btn btn-default btn-mini pull-right"
                                   href="<?php echo base_url("auth/logout"); ?>">Sign Out</a>
							</span>
                            </li>
                        </ul>
                    </li>
                    <!-- // Profile / Logout menu END -->
                </ul>
                <!-- // Top Menu Right END -->
                <ul class="topnav pull-right hidden-phone">
                    <li><a href="<?php echo base_url("message"); ?>" class="glyphicons envelope single-icon"><i></i><span class="badge fix badge-primary"><?php echo $msg_count;?></span></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
