<!DOCTYPE html>
<!--[if lt IE 7]>
<html
    class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-dropdown sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if IE 7]>
<html
    class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-dropdown sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if IE 8]>
<html
    class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-dropdown sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if gt IE 8]>
<html
    class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-dropdown sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html
    class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-dropdown sidebar-width-mini sidebar-hat">
<!-- <![endif]-->
<head>
    <title><?php echo $title; ?> - Sombath Team</title>

    <!-- Meta -->
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE"/>

    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/common/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/common/bootstrap/css/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Glyphicons Font Icons -->
    <link href="<?php echo base_url() ?>assets/common/theme/fonts/glyphicons/css/glyphicons.css" rel="stylesheet"/>

    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/common/theme/fonts/font-awesome/css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet"
          href="<?php echo base_url()?>assets/common/theme/fonts/font-awesome/css/font-awesome-ie7.min.css"><![endif]-->

    <!-- Uniform Pretty Checkboxes -->
    <link
        href="<?php echo base_url() ?>assets/common/theme/scripts/plugins/forms/pixelmatrix-uniform/css/uniform.default.css"
        rel="stylesheet"/>

    <!-- PrettyPhoto -->
    <link href="<?php echo base_url() ?>assets/common/theme/scripts/plugins/gallery/prettyphoto/css/prettyPhoto.css"
          rel="stylesheet"/>

    <!-- JQuery -->
    <script src="<?php echo base_url() ?>assets/js/jquery-1.10.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/login.js"></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url()?>assets/common/theme/scripts/plugins/system/html5shiv.js"></script>
    <![endif]-->

    <!-- Main Theme Stylesheet :: CSS -->
    <link href="<?php echo base_url() ?>assets/common/theme/css/style-default-menus-light.css?1374506526"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/style.css" rel="stylesheet" type="text/css"/>

    <!-- FireBug Lite -->
    <!-- <script src="https://getfirebug.com/firebug-lite-debug.js"></script> -->
    <link href="<?php echo base_url() ?>assets/common/bootstrap/extend/jasny-fileupload/css/fileupload.css" rel="stylesheet">
    <!-- LESS.js Library -->
    <script src="<?php echo base_url() ?>assets/common/theme/scripts/plugins/system/less.min.js"></script>

    <!-- Global -->
    <script>
        //<![CDATA[
        var basePath = '',
            commonPath = 'common/';

        // colors
        var primaryColor = '#e5412d',
            dangerColor = '#b55151',
            successColor = '#609450',
            warningColor = '#ab7a4b',
            inverseColor = '#45484d';

        var themerPrimaryColor = primaryColor;
        //]]>
    </script>
</head>
<body class="document-body login">