<?php
define("ROOT_PATH", dirname(__DIR__));
include_once ROOT_PATH . "/template/header.php";
include_once ROOT_PATH . "/template/menu.php";
?>

<h1>My Photos</h1>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget widget-tabs widget-tabs-gray widget-tabs-double-2 border-bottom-none">

        <div class="widget-body">
                <div class="tab-content">
                    <div class="tab-pane active widget-body-regular padding-none" id="overview">
                        <div class="row-fluid row-merge">
                            <div class="span12 containerBg innerTB">

                                <div class="innerLR">
                                    <div class="row-fluid innerB">
                                        <div class="widget widget-gallery" data-toggle="collapse-widget">
                                            <div class="widget-head"><h4 class="heading">Profile Photos</h4></div>
                                            <div class="widget-body">
                                                <!-- Gallery Layout -->
                                                <div class="gallery gallery-2">
                                                    <ul class="row-fluid" data-toggle="modal-gallery" data-target="#modal-gallery" id="gallery-4" data-delegate="#gallery-4">
                                                        <?php if($data["img_url"]) {?>
                                                        <li class="span3 hidden-phone"><a class="thumb" href="<?php echo base_url(); ?>assets/uploads/<?php echo $data["img_url"]; ?>" data-gallery="gallery"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $data["img_url"]; ?>" alt="photo" /></a></li>
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/7.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/7.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/6.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/6.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/5.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/5.jpg" alt="photo" /></a></li>-->
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                                <!-- // Gallery Layout END -->
                                            </div>
                                        </div>
                                        <div class="widget widget-gallery" data-toggle="collapse-widget">
                                            <div class="widget-head"><h4 class="heading">Checkin Photos</h4></div>
                                            <div class="widget-body">
                                                <!-- Gallery Layout -->
                                                <div class="gallery gallery-2">
                                                    <ul class="row-fluid" data-toggle="modal-gallery" data-target="#modal-gallery" id="gallery-4" data-delegate="#gallery-4">
                                                        <?php
                                                        foreach($check_in as $row) {
                                                            echo '<li class="span3 hidden-phone"><a class="thumb" href="'. base_url().'assets/uploads/'.$row['invoice_url'].'" data-gallery="gallery"><img src="'. base_url().'assets/uploads/'.$row['invoice_url'].'" alt="photo" /></a></li>';
                                                        }
                                                        ?>
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/8.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/8.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/7.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/7.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/6.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/6.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/5.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/5.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3 hidden-phone"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/4.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/4.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/3.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/3.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/2.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/2.jpg" alt="photo" /></a></li>-->
<!--                                                        <li class="span3"><a class="thumb" href="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/1.jpg" data-gallery="gallery"><img src="--><?php //echo base_url(); ?><!--assets/common/theme/images/gallery-2/1.jpg" alt="photo" /></a></li>-->
                                                    </ul>
                                                </div>
                                                <!-- // Gallery Layout END -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
    <!-- // Widget END -->
</div>
</div>
<!-- // Content END -->
<?php
include_once ROOT_PATH . "/template/menu-footer.php";
include_once ROOT_PATH . "/template/footer.php";
?>
