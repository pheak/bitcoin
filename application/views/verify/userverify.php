<?php
define("ROOT_PATH", dirname(__DIR__));
include_once ROOT_PATH . "/template/header.php";
include_once ROOT_PATH . "/template/menu.php";
?>

<h1>Verify</h1>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget widget-tabs widget-tabs-gray widget-tabs-double-2 border-bottom-none">

        <div class="widget-body">
          <div class="tab-content">
            <div class="tab-pane active widget-body-regular padding-none" id="overview">
              <div class="row-fluid row-merge">
                <div class="span12 containerBg innerTB">
                  <div class="innerLR">
                    <div class="row-fluid innerB">
                      <div class="widget widget-heading-simple widget-body-simple">
                        <div class="widget-body">
                          <div class="widget widget-tabs widget-tabs-icons-only-2 widget-activity margin-none widget-tabs-gray">
                            <div class="widget-body">
                              <div class="tab-content">
                                <div class="row-fluid innerB">
                                  <div class="span12">
                                       <?php echo form_open_multipart('verify/update');?>
                                      <!-- About -->
                                      <div class="widget widget-heading-simple widget-body-gray margin-none">
                                          <div class="widget-body">
                                              <div class="row-fluid innerB">
                                                  <div class="span2">
                                                      <div class="col-md-12 fileinput fileinput-new blog-center" data-provides="fileinput">
                                                          <span class="img-verify">
                                                              <img style="width:100%;margin-bottom:10px;" id="preview1" alt="User Pic" src="<?php echo (!empty($verify[0]))?base_url().'assets/uploads/'.$verify[0]['img_url']:base_url().'assets/img/invoice.png';?>" class="">
                                                              <?php if(!empty($verify[0]) && $verify[0]['verify']==1){ ?><span class="verify"></spa> <?php } ?>
                                                                  <?php if(!empty($verify[0]) && $verify[0]['verify']==2){ ?><span class="unverify"></spa> <?php } ?>
                                                          </span>
                                                           <?php if(empty($verify[0]) || $verify[0]['verify']!=1){ ?>
                                                          <div class="fileupload fileupload-new margin-none" style="text-align: center;" data-provides="fileupload">
                                                              <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new">Select file</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                <input class="margin-none" type="file" name="image1" onchange="previewImage(this, 'preview1')" />
                                                              <span class="fileupload-preview"></span>
                                                              <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
                                                          </div>
                                                          <span class="fileinput-filename"></span>
                                                           <?php } ?>
                                                      </div>
                                                  </div>
                                                  <div class="span2">
                                                      <div class="col-md-12 fileinput fileinput-new blog-center" data-provides="fileinput">
                                                          <span class="img-verify">
                                                                <img style="width:100%;margin-bottom:10px;" id="preview2" alt="User Pic" src="<?php echo (!empty($verify[1]))?base_url().'assets/uploads/'.$verify[1]['img_url']:base_url().'assets/img/invoice.png';?>" class="">
                                                                <?php if(!empty($verify[1]) && $verify[1]['verify']==1){ ?><span class="verify"></spa> <?php } ?>
                                                                <?php if(!empty($verify[1]) && $verify[1]['verify']==2){ ?><span class="unverify"></spa> <?php } ?>

                                                          </span>
                                                          <?php if(empty($verify[1]) || $verify[1]['verify']!=1){ ?>
                                                          <div class="fileupload fileupload-new margin-none" style="text-align: center;" data-provides="fileupload">
                                                              <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new">Select file</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                <input class="margin-none" type="file" name="image2" onchange="previewImage(this, 'preview2')" />
                                                              <span class="fileupload-preview"></span>
                                                              <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
                                                          </div>
                                                          <span class="fileinput-filename"></span>
                                                           <?php } ?>
                                                      </div>
                                                  </div>
                                                  <div class="span2">
                                                      <div class="col-md-12 fileinput fileinput-new blog-center" data-provides="fileinput">
                                                          <span class="img-verify">
                                                              <img style="width:100%;margin-bottom:10px;" id="preview3" alt="User Pic" src="<?php echo (!empty($verify[2]))?base_url().'assets/uploads/'.$verify[2]['img_url']:base_url().'assets/img/invoice.png';?>" class="">
                                                              <?php if(!empty($verify[2]) && $verify[2]['verify']==1){ ?><span class="verify"></spa> <?php } ?>
                                                                  <?php if(!empty($verify[2]) && $verify[2]['verify']==2){ ?><span class="unverify"></spa> <?php } ?>
                                                          </span>
                                                          <?php if(empty($verify[2]) || $verify[2]['verify']!=1){ ?>
                                                          <div class="fileupload fileupload-new margin-none" style="text-align: center;" data-provides="fileupload">
                                                              <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new">Select file</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                <input class="margin-none" type="file" name="image3" onchange="previewImage(this, 'preview3')" />
                                                              <span class="fileupload-preview"></span>
                                                              <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
                                                          </div>
                                                          <span class="fileinput-filename"></span>
                                                          <?php } ?>
                                                      </div>
                                                  </div>
                                                  <div class="span2">
                                                      <div class="col-md-12 fileinput fileinput-new blog-center" data-provides="fileinput">
                                                          <span class="img-verify">
                                                              <img style="width:100%;margin-bottom:10px;" id="preview4" alt="User Pic" src="<?php echo (!empty($verify[3]))?base_url().'assets/uploads/'.$verify[3]['img_url']:base_url().'assets/img/invoice.png';?>" class="">
                                                              <?php if(!empty($verify[3]) && $verify[3]['verify']==1){ ?><span class="verify"></spa> <?php } ?>
                                                                  <?php if(!empty($verify[3]) && $verify[3]['verify']==2){ ?><span class="unverify"></spa> <?php } ?>
                                                          </span>
                                                          <?php if(empty($verify[3]) || $verify[3]['verify']!=1){ ?>
                                                          <div class="fileupload fileupload-new margin-none" style="text-align: center;" data-provides="fileupload">
                                                              <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new">Select file</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                <input class="margin-none" type="file" name="image4" onchange="previewImage(this, 'preview4')" />
                                                              <span class="fileupload-preview"></span>
                                                              <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
                                                          </div>
                                                          <span class="fileinput-filename"></span>
                                                           <?php } ?>
                                                      </div>
                                                  </div>
                                              </div>
                                              <button type="submit" class="btn btn-icon-stacked  btn-success glyphicons user_add btn-user-save"><i></i>Save</button>
                                        </form>
                                    </div>
                                </div>
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <!-- // Widget END -->
      <script type="text/javascript">
          function previewImage(input, preview) {
              var preview = document.getElementById(preview);
              if (input.files && input.files[0]) {
                  var reader = new FileReader();
                  reader.onload = function (e) {
                      preview.setAttribute('src', e.target.result);
                  }
                  reader.readAsDataURL(input.files[0]);
              }
          }
      </script>
  </div>
</div>
<!-- // Content END -->
<?php
include_once ROOT_PATH . "/template/menu-footer.php";
include_once ROOT_PATH . "/template/footer.php";
?>
