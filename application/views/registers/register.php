<!-- Wrapper -->
<div id="login">

    <div class="wrapper signup">

        <h1 class="glyphicons lock">SOMBATH TEAM <i></i></h1>

        <!-- Box -->
        <div class="widget widget-heading-simple">

            <div class="widget-head">
                <h3 class="heading">Create Account</h3>
                <div class="pull-right">
                    Already a member?
                    <a href="auth/login" class="btn btn-inverse btn-mini">Sign in</a>
                </div>
            </div>
            <div class="widget-body">

                <!-- Form -->
                <?php echo form_open('register', array('id' => 'register')); ?>
                <p class="success"><?php echo $success_msg;?> </p>
                <!-- Row -->
                <div class="row-fluid row-merge">

                    <!-- Column -->
                    <div class="span6">
                        <div class="innerR">
                            <label class="strong">Username</label>
                            <?php echo form_error('username', '<div class="error">', '</div>'); ?>
                            <input type="text" name="username" class="input-block-level" placeholder="Your Username" value="<?php echo $data['username'];?>" required/>
                            <label class="strong">Password</label>
                            <input type="password" name="pwd" class="input-block-level" placeholder="Your Password" required/>
                            <label class="strong">Confirm Password</label>
                            <input type="password" class="input-block-level" placeholder="Confirm Password" required/>
                            <label class="strong">Email</label>
                            <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                            <input type="text" name="email" class="input-block-level" placeholder="Your Email Address" value="<?php echo $data['email'];?>" required/>
                            <label class="strong">Phone</label>
                            <input type="text" name="phone" class="input-block-level" placeholder="Your Phone Number" value="<?php echo $data['phone'];?>" required/>
                            <label class="strong">Sex</label>
                            <input type="text" name="sex" class="input-block-level" placeholder="Sex" value="<?php echo $data['sex'];?>" required/>
                            <label class="strong">Age</label>
                            <input type="text" name="age" class="input-block-level" placeholder="Age" value="<?php echo $data['age'];?>" required/>
                            <label class="strong">Sponser</label>
                            <input type="text" name="sponser" class="input-block-level" placeholder="Sponser" value="<?php echo $data['sponser'];?>" required/>
                        </div>
                    </div>
                    <!-- // Column END -->

                    <!-- Column -->
                    <div class="span6">
                        <div class="innerL">
                            <label class="strong">Home No</label>
                            <input type="text" name="homeno" class="input-block-level" placeholder="Your Home Number" value="<?php echo $data['homeno'];?>" required/>
                            <label class="strong">Street</label>
                            <input type="text" name="street" class="input-block-level" placeholder="Your Street Number" value="<?php echo $data['street'];?>" required/>
                            <label class="strong">Phum</label>
                            <input type="text" name="phum" class="input-block-level" placeholder="Your Phum" value="<?php echo $data['phum'];?>" required/>
                            <label class="strong">Sankat</label>
                            <input type="text" name="sangkat" class="input-block-level" placeholder="Your Sankat" value="<?php echo $data['sangkat'];?>" required/>
                            <label class="strong">Khan</label>
                            <input type="text" name="khan" class="input-block-level" placeholder="Your Khan" value="<?php echo $data['khan'];?>" required/>
                            <label class="strong">City</label>
                            <input type="text" name="city" class="input-block-level" placeholder="Your City" value="<?php echo $data['city'];?>" required/>
                            <label class="strong">Country</label>
                            <input type="text" name="country" class="input-block-level" placeholder="Your Country" value="<?php echo $data['country'];?>" required/>
                            <button type="submit" class="btn btn-icon-stacked btn-block btn-success glyphicons user_add"><i></i><span>Create account and</span><span class="strong">Join Sombath Team now</span></button>
                        </div>
                    </div>
                    <!-- // Column END -->

                </div>
                <!-- // Row END -->

                </form>
                <!-- // Form END -->

            </div>
            <!-- // Box END -->
