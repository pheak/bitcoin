<?php
define("ROOT_PATH", dirname(__DIR__));
include_once ROOT_PATH . "/template/header.php";
include_once ROOT_PATH . "/template/menu.php";
?>

<h1>My Message</h1>
<div class="innerLR">

    <!-- Widget -->
    <div class="widget widget-tabs widget-tabs-gray widget-tabs-double-2 border-bottom-none">

        <div class="widget-body">
                <div class="tab-content">
                    <div class="tab-pane active widget-body-regular padding-none" id="overview">
                        <div class="row-fluid row-merge">
                            <div class="span12 containerBg innerTB">

                                <div class="innerLR">
                                    <div class="row-fluid innerB">
                                        <div class="widget widget-heading-simple widget-body-simple">
                                            <div class="widget-body">
                                                <div
                                                    class="widget widget-tabs widget-tabs-icons-only-2 widget-activity margin-none widget-tabs-gray">
                                                    <div class="widget-body">
                                                        <div class="tab-content">
                                                            <!-- Filter Messages Tab -->
                                                            <div class="tab-pane active" id="filterMessagesTab">
                                                                <ul class="list">

                                                                    <?php
                                                                    foreach($messages as $row) { ?>
                                                                        <!-- Activity item -->
                                                                        <li class="double">
                                                                            <span class="glyphicons activity-icon icon-bullhorn"><i></i></span>
                                                                            <span class="ellipsis">
                                                                                <?php echo $row["title"]; ?>: <?php echo $row["message"]; ?>
                                                                                <span class="meta">on <?php
                                                                                    $date = new DateTime($row["date"]);
                                                                                    echo date_format($date, 'M d, Y');; ?></span>
                                                                            </span>
                                                                            <div class="clearfix"></div>
                                                                        </li>
                                                                    <?php } ?>

                                                                </ul>
                                                            </div>
                                                            <!-- // Filter Messages Tab END -->
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
    <script type="text/javascript">
        function previewImage(input) {
            var preview = document.getElementById('preview');
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    preview.setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <!-- // Widget END -->
</div>
</div>
<!-- // Content END -->
<?php
include_once ROOT_PATH . "/template/menu-footer.php";
include_once ROOT_PATH . "/template/footer.php";
?>
